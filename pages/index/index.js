//index.js
//获取应用实例
let timer;
let numAi = 0;
Page({
  data: {
    btnState: false,
    winNum: 0,
    gameResult: '',
    imageAiSrc: '',
    imageUserSrc: '/pages/image/4.jpg',
    srcs: ['/pages/image/1.png',
      '/pages/image/2.png',
      '/pages/image/3.png']
  },
  onLoad: function (options) {
    let oldWinNum=wx.getStorageSync('winNum');
    if(oldWinNum!=null&&oldWinNum!=''){
      this.setData({
        winNum:oldWinNum
      })
    }
    this.timeGo();
  },
  changeForChoose(e) {
    if (this.data.btnState) {
      return;
    }
    console.log(e);

    this.setData({
      imageUserSrc: this.data.srcs[e.currentTarget.id]
    })
    clearInterval(timer);
    let user = this.data.imageUserSrc;
    let ai = this.data.imageAiSrc;
    let num = this.data.winNum;
    let str = '你输了！';
    if (user == '/pages/image/1.png' && ai == '/pages/image/2.png') {
      num++;
      str = "你赢了！";
      wx.setStorageSync('winNum', num)
    }
    if (user == '/pages/image/2.png' && ai == '/pages/image/3.png') {
      num++;
      str = "你赢了！";
      wx.setStorageSync('winNum', num)
    }
    if (user == '/pages/image/3.png' && ai == '/pages/image/1.png') {
      num++;
      str = "你赢了！";
      wx.setStorageSync('winNum', num)
    }
    if (user == ai) {
      str = '平局!';
    }
    this.setData({
      winNum: num,
      gameResult: str,
      btnState: true,
    })
  },
  timeGo() {
    timer = setInterval(this.move, 100);
  },
  move() {

    // if (numAi >= 3) {
    //   numAi = 0;
    // }
    //随机数
    numAi = parseInt(Math.floor(Math.random() * 3) );
    this.setData({ imageAiSrc: this.data.srcs[numAi] })
    // numAi++;

  },
  again() {
    if (this.data.btnState==false) {
      return;
    }
    this.timeGo();
    this.setData({

      btnState: false,
      gameResult: '',
      imageUserSrc: '/pages/image/4.jpg',
    })
  },
  onReady: function () {

  },
  onShow: function () {

  },
  onHide: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  },
  onShareAppMessage: function () {
    return {
      title: '猜拳小游戏',
     
    }
  }

})